import { GetStaticProps, InferGetStaticPropsType } from "next";

import { getAllChapters } from "@/services/chapter-service";
import { Chapter } from "@/models/chapter";
import { NavBarChapters } from "@/components/meditator/navbar/NavBarChapters";
import { KoanPlayer } from "@/components/meditator/koan-player/KoanPlayer";
import {
  SelectedKoan,
  SelectedKoanContext,
} from "@/context/selected-koan-context";
import { useEffect, useState } from "react";
import {
  getKoansFromStorage,
  getNextKoan,
} from "@/services/storage-client/storage-client-service";
import { StoredKoan, StoredKoansContext } from "@/context/passed-koans-context";
import { ChaptersContext } from "@/context/chapters-context";
import { ResetKoans } from "@/components/meditator/reset-koans/ResetKoans";

export const getStaticProps: GetStaticProps<{
  chapters: Chapter[];
}> = async () => {
  try {
    const chapters = await getAllChapters();
    return {
      props: {
        chapters,
      },
    };
  } catch (ex: any) {
    return {
      notFound: true,
    };
  }
};

export default function Home({
  chapters,
}: InferGetStaticPropsType<typeof getStaticProps>) {
  const [selectedKoan, setSelectedKoan] = useState<SelectedKoan>();
  const [storedKoans, setStoredKoans] = useState<StoredKoan[]>([]);
  const [isFinished, setIsFinished] = useState(false);

  useEffect(() => {
    const nextKoan = getNextKoan(chapters);
    if (nextKoan !== undefined) {
      setSelectedKoan({
        chapterNumber: nextKoan.chapterNumber,
        koanOrder: nextKoan.koanOrder,
      });
    }
  }, []);

  useEffect(() => {
    const nextKoan = getNextKoan(chapters);
    setIsFinished(nextKoan === undefined);
  }, [storedKoans]);

  useEffect(() => {
    const storedKoans: StoredKoan[] = getKoansFromStorage().map(
      (koanToStore) => {
        return {
          chapterNumber: koanToStore.chapterNumber,
          koanOrder: koanToStore.koanOrder,
          isPassed: koanToStore.isPassed,
        } as StoredKoan;
      }
    );
    setStoredKoans(storedKoans);
  }, []);

  return (
    <ChaptersContext.Provider value={{ chapters }}>
      <SelectedKoanContext.Provider
        value={{
          selectedKoan: selectedKoan,
          setSelectedKoan: setSelectedKoan,
        }}
      >
        <StoredKoansContext.Provider
          value={{ storedKoans: storedKoans, setStoredKoans: setStoredKoans }}
        >
          <nav className="top-bar">Mongo Koans</nav>
          <div className="player-container">
            <NavBarChapters />
            <div className="display-flex flex-direction-column justify-center width-max">
              {storedKoans.length > 0 && isFinished && <ResetKoans />}
              {selectedKoan && (
                <KoanPlayer
                  chapterNumber={selectedKoan.chapterNumber}
                  koanOrder={selectedKoan.koanOrder}
                />
              )}
            </div>
          </div>
        </StoredKoansContext.Provider>
      </SelectedKoanContext.Provider>
    </ChaptersContext.Provider>
  );
}
