import { Koan } from "@/models/koan";
import { getOneKoan } from "@/services/chapter-service";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { chapterNumber, koanOrder } = req.query;
  if (typeof chapterNumber === "string" && typeof koanOrder === "string") {
    const koan = await getOneKoan(parseInt(chapterNumber), parseInt(koanOrder));
    res.status(200).json(koan);
  } else {
    res.status(400).json({ text: "bad params" });
  }
}
