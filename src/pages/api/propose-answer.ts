import { proposeAnswer } from "@/services/chapter-service";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Answer | Error>
) {
  const { commands } = req.body;
  const { chapterNumber, koanOrder } = req.query;

  if (typeof chapterNumber === "string" && typeof koanOrder === "string") {
    const response = await proposeAnswer(
      commands,
      parseInt(chapterNumber),
      parseInt(koanOrder)
    );
    res.status(200).json(response);
  } else {
    console.error(
      `chapter number '${chapterNumber}' or koan order '${koanOrder}' are not number params`
    );
    res.status(400).json({ message: "Bad params", name: "Bad params" });
  }
}
