import { MouseEventHandler, memo, useMemo } from "react";
import styles from "./CustomButton.module.css";

type CustomButtonProps = {
  children: JSX.Element;
  onClick: MouseEventHandler<HTMLButtonElement> | undefined;
  customClasName?: string;
  typeButton: "normal" | "success";
  disabled?: boolean;
};

const CustomButtonComponent = ({
  children,
  onClick,
  customClasName,
  typeButton,
  disabled = false,
}: CustomButtonProps) => {
  const styleButton = useMemo(() => {
    switch (typeButton) {
      case "normal":
        return styles.normalButton;
      default:
        return styles.successButton;
    }
  }, [typeButton]);
  return (
    <button
      className={`${styleButton}${customClasName ? " " + customClasName : ""}`}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

export const CustomButton = memo(CustomButtonComponent);
