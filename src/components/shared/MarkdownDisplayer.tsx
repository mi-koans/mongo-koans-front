import { useEffect, useState } from "react";
import { remark } from "remark";
import html from "remark-html";

const processorMD = remark().use(html);

type MarkdownDisplayerProps = {
  markdown: string;
};
export const MarkdownDisplayer = ({ markdown }: MarkdownDisplayerProps) => {
  const [content, setContent] = useState("");

  useEffect(() => {
    processorMD.process(markdown).then((htmlContent) => {
      setContent(htmlContent.toString());
    });
  }, []);

  return (
    <div
      style={{ lineHeight: "1.5rem", textAlign: "justify" }}
      dangerouslySetInnerHTML={{ __html: content }}
    />
  );
};
