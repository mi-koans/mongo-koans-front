import styles from "./NavBarChapters.module.css";
import { ChapterContainer } from "./ChapterContainer";
import { useContext, useEffect, useState } from "react";
import { ChaptersContext } from "@/context/chapters-context";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { SelectedKoanContext } from "@/context/selected-koan-context";

export const NavBarChapters = () => {
  const [dropDown, setDropDown] = useState(false);
  const { chapters } = useContext(ChaptersContext);
  const { selectedKoan } = useContext(SelectedKoanContext);

  const [selectedChapterNumber, setSelectedChapterNumber] = useState(
    selectedKoan?.chapterNumber
  );

  useEffect(() => {
    setSelectedChapterNumber(selectedKoan?.chapterNumber);
  }, [selectedKoan]);

  const handleDropDown = () => {
    setDropDown(!dropDown);
  };

  return (
    <div className={styles.navBarChapters}>
      <nav>
        <div className={styles.title} onClick={handleDropDown}>
          <span>Koans Menu</span>
          <span>
            {dropDown ? (
              <FontAwesomeIcon icon={faChevronDown} rotation={180} />
            ) : (
              <FontAwesomeIcon icon={faChevronDown} />
            )}
          </span>
        </div>
        <ol
          className={`${styles.chapters} ${
            !dropDown ? styles.dropUpChapters : ""
          }`}
        >
          {chapters.map((chapter) => (
            <ChapterContainer
              chapter={chapter}
              key={`${chapter.number}`}
              selectedChapterNumber={selectedChapterNumber}
              setSelectedChapterNumber={setSelectedChapterNumber}
            />
          ))}
        </ol>
      </nav>
    </div>
  );
};
