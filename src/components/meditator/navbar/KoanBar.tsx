import { SelectedKoanContext } from "@/context/selected-koan-context";
import { SummaryKoans } from "@/models/chapter";
import { useContext } from "react";
import styles from "./KoanBar.module.css";
import { StoredKoansContext } from "@/context/passed-koans-context";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";

type KoanLineProps = {
  chapterNumber: number;
  koan: SummaryKoans;
};
export const KoanLine = ({ chapterNumber, koan }: KoanLineProps) => {
  const { setSelectedKoan, selectedKoan } = useContext(SelectedKoanContext);
  const { storedKoans: passedKoans } = useContext(StoredKoansContext);

  function getKoanClassName(chapterNumber: number, koanOrder: number) {
    return selectedKoan &&
      selectedKoan.chapterNumber === chapterNumber &&
      selectedKoan.koanOrder === koanOrder
      ? ` ${styles.koanSelected}`
      : " pointer";
  }

  const handleSelectKoan = () => {
    setSelectedKoan({
      chapterNumber,
      koanOrder: koan.order,
    });
  };

  function isKoanPassed() {
    return passedKoans.some(
      (passedKoan) =>
        passedKoan.chapterNumber === chapterNumber &&
        passedKoan.koanOrder === koan.order &&
        passedKoan.isPassed
    );
  }

  return (
    <li key={`koan-${chapterNumber}-${koan.order}`}>
      <button
        onClick={handleSelectKoan}
        className={styles.koan + getKoanClassName(chapterNumber, koan.order)}
        disabled={
          selectedKoan?.chapterNumber === chapterNumber &&
          selectedKoan.koanOrder === koan.order
        }
      >
        <FontAwesomeIcon
          icon={faCheck}
          className={isKoanPassed() ? "" : "hidden"}
          color="#4DBB5F"
        />
        {koan.title}
      </button>
    </li>
  );
};
