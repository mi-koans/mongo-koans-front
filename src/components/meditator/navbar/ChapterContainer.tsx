import { Chapter } from "@/models/chapter";
import { KoanLine } from "./KoanBar";
import styles from "./ChapterContainer.module.css";
import { Dispatch, SetStateAction, memo } from "react";
import { faCaretRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

type ChapterListProps = {
  chapter: Chapter;
  selectedChapterNumber: number | undefined;
  setSelectedChapterNumber: Dispatch<SetStateAction<number | undefined>>;
};
const ChapterContainerComponent = ({
  chapter,
  selectedChapterNumber,
  setSelectedChapterNumber,
}: ChapterListProps) => {
  function handleSelectChapterNumber() {
    setSelectedChapterNumber(chapter.number);
  }

  return (
    <ul key={`chapter-${chapter.number}`} className="no-padding">
      <button
        className={`${styles.chapterTitle} ${
          selectedChapterNumber &&
          selectedChapterNumber !== chapter.number &&
          "pointer"
        }`}
        onClick={handleSelectChapterNumber}
        disabled={selectedChapterNumber === chapter.number}
      >
        {selectedChapterNumber && (
          <>
            {selectedChapterNumber === chapter.number ? (
              <FontAwesomeIcon icon={faCaretRight} rotation={90} />
            ) : (
              <FontAwesomeIcon icon={faCaretRight} />
            )}
          </>
        )}

        <div>{chapter.name}</div>
      </button>
      <div>
        {selectedChapterNumber === chapter.number && (
          <ol className="no-padding">
            {chapter.summaryKoans.map((koan) => (
              <KoanLine
                chapterNumber={chapter.number}
                koan={koan}
                key={`${chapter.number} - ${koan.order}`}
              />
            ))}
          </ol>
        )}
      </div>
    </ul>
  );
};

export const ChapterContainer = memo(ChapterContainerComponent);
