import { StoredKoansContext } from "@/context/passed-koans-context";
import { resetStorage } from "@/services/storage-client/storage-client-service";
import { useCallback, useContext } from "react";
import styles from "./ResetKoans.module.css";
import { SelectedKoanContext } from "@/context/selected-koan-context";
import { CustomButton } from "@/components/shared/ui/CustomButton";

export const ResetKoans = () => {
  const { setStoredKoans } = useContext(StoredKoansContext);
  const { setSelectedKoan } = useContext(SelectedKoanContext);

  const handleResetAll = useCallback(() => {
    resetStorage();
    setStoredKoans([]);
    setSelectedKoan({ chapterNumber: 1, koanOrder: 1 });
  }, []);

  return (
    <div>
      <div className={styles.resetKoanContainer}>
        <span>Congratulations you finish mongo koans !</span>
        <CustomButton
          onClick={handleResetAll}
          customClasName={styles.resetButton}
          typeButton="normal"
        >
          <>Reset all koans</>
        </CustomButton>
      </div>
    </div>
  );
};
