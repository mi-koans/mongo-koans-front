import { StoredKoan, StoredKoansContext } from "@/context/passed-koans-context";
import { KoanToStore } from "@/models/store-koans";
import {
  getKoansFromStorage,
  getNextKoan,
  saveOneKoanInStorage,
} from "@/services/storage-client/storage-client-service";
import CodeMirror from "@uiw/react-codemirror";
import { memo, useEffect, useContext, useState } from "react";
import styles from "./ResultKoan.module.css";
import { ChaptersContext } from "@/context/chapters-context";
import { SelectedKoanContext } from "@/context/selected-koan-context";
import { CustomButton } from "@/components/shared/ui/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faXmark } from "@fortawesome/free-solid-svg-icons";

type ResultKoanProps = {
  chapterNumber: number;
  koanOrder: number;
  code: string;
  answer: Answer;
};
const ResultKoanComponent = ({
  chapterNumber,
  koanOrder,
  code,
  answer,
}: ResultKoanProps) => {
  const [hasNext, setHasNext] = useState(false);
  const { setStoredKoans } = useContext(StoredKoansContext);
  const { chapters } = useContext(ChaptersContext);
  const { setSelectedKoan } = useContext(SelectedKoanContext);

  useEffect(() => {
    if (answer) {
      const koanToStore: KoanToStore = {
        chapterNumber,
        koanOrder,
        code,
        isPassed: answer.proposeCorrect,
      };
      saveOneKoanInStorage(koanToStore);
      const storedKoans = getKoansFromStorage().map<StoredKoan>((koan) => {
        return {
          chapterNumber: koan.chapterNumber,
          koanOrder: koan.koanOrder,
          isPassed: koan.isPassed,
        };
      });
      setStoredKoans(storedKoans);
    }
  }, [answer]);

  useEffect(() => {
    if (answer) {
      setHasNext(getNextKoan(chapters) !== undefined);
    }
  }, [answer]);

  const handleNextKoan = () => {
    const nextKoan = getNextKoan(chapters);
    if (nextKoan) {
      setSelectedKoan({
        chapterNumber: nextKoan.chapterNumber,
        koanOrder: nextKoan.koanOrder,
      });
    }
  };

  return (
    <div className={styles.resultKoan}>
      <CodeMirror value={answer.commandResult.message} minHeight="100px" />
      {answer.proposeCorrect ? (
        <div className={`${styles.container}`}>
          <div className={` ${styles.passedKoandAndNextButton}`}>
            <FontAwesomeIcon icon={faCheck} />
            <div>You passed the koan</div>
          </div>

          <CustomButton
            disabled={!hasNext}
            onClick={handleNextKoan}
            typeButton="success"
          >
            <>Next</>
          </CustomButton>
        </div>
      ) : (
        <div className={`${styles.container} ${styles.wrongAnswerContainer}`}>
          <FontAwesomeIcon icon={faXmark} />
          <span>Your answer is wrong</span>
        </div>
      )}
    </div>
  );
};

export const ResultKoan = memo(ResultKoanComponent);
