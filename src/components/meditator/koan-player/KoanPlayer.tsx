import useSWR, { Fetcher } from "swr";
import CodeMirror from "@uiw/react-codemirror";
import { MarkdownDisplayer } from "@/components/shared/MarkdownDisplayer";
import {
  memo,
  useEffect,
  useState,
  useContext,
  useMemo,
  useCallback,
} from "react";
import { Koan } from "@/models/koan";
import { ResultKoan } from "./ResultKoan";
import {
  getKoansFromStorage,
  getStoredKoan,
  removeOneKoanInStorage,
} from "@/services/storage-client/storage-client-service";
import useSWRMutation from "swr/mutation";
import styles from "./KoanPlayer.module.css";
import { StoredKoansContext } from "@/context/passed-koans-context";
import { CustomButton } from "@/components/shared/ui/CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlay, faSpinner } from "@fortawesome/free-solid-svg-icons";

const getOneKoanFetcher: Fetcher<
  Koan,
  { chapterNumber: number; koanOrder: number }
> = ({ chapterNumber, koanOrder }) =>
  fetch(
    `/api/get-one-koan?chapterNumber=${chapterNumber}&koanOrder=${koanOrder}`
  ).then((r) => r.json());

const proposeAnswerFetcher: Fetcher<
  Answer,
  { commands: string; chapterNumber: number; koanOrder: number }
> = ({ commands, chapterNumber, koanOrder }) =>
  fetch(
    `/api/propose-answer?chapterNumber=${chapterNumber}&koanOrder=${koanOrder}`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ commands }),
    }
  ).then((r) => r.json());

type KoanPlayerProps = {
  chapterNumber: number;
  koanOrder: number;
};

const DEFAULT_CODE_VALUE = "// write mongo commands here";

const KoanPlayerComponent = ({ chapterNumber, koanOrder }: KoanPlayerProps) => {
  const [code, setCode] = useState<string>(DEFAULT_CODE_VALUE);
  const [solution, setSolution] = useState("");
  const { storedKoans, setStoredKoans } = useContext(StoredKoansContext);
  const {
    data: currentKoan,
    error,
    isLoading: isKoanLoading,
  } = useSWR({ chapterNumber, koanOrder }, getOneKoanFetcher);

  const [deviceSize, changeDeviceSize] = useState(window.innerWidth);

  const {
    data: answer,
    trigger,
    error: errorAnswer,
    isMutating,
    reset,
  } = useSWRMutation(
    { commands: code, chapterNumber, koanOrder },
    proposeAnswerFetcher
  );

  useEffect(() => {
    const resizeW = () => changeDeviceSize(window.innerWidth);

    window.addEventListener("resize", resizeW); // Update the width on resize

    return () => window.removeEventListener("resize", resizeW);
  }, [deviceSize]);

  useEffect(() => {
    const storedKoan = getStoredKoan(chapterNumber, koanOrder);
    if (storedKoan) {
      setCode(storedKoan.code);
    } else {
      setCode(DEFAULT_CODE_VALUE);
    }
  }, [chapterNumber, koanOrder]);

  useEffect(() => {
    const storedKoans = getKoansFromStorage();
    if (storedKoans.length === 0) {
      setCode(DEFAULT_CODE_VALUE);
    }
  }, [storedKoans]);

  useEffect(() => {
    reset();
  }, [chapterNumber, koanOrder]);

  useEffect(() => {
    if (!isMutating && storedKoans.length === 0 && answer) {
      reset();
    }
  }, [storedKoans]);

  const handleCodeChange = (newCode: string) => {
    setCode(newCode);
  };

  const handleClick = useCallback(() => {
    trigger();
  }, []);

  const handleShowSolution = () => {
    if (currentKoan) {
      setSolution(currentKoan?.solution);
    }
  };
  const handleHideSolution = () => {
    setSolution("");
  };
  const handleResetPassedKoan = () => {
    if (concernedKoans) {
      const filteredPassedKoans = storedKoans.filter(
        (koan) =>
          koan.chapterNumber !== chapterNumber || koan.koanOrder !== koanOrder
      );
      setStoredKoans(filteredPassedKoans);
      removeOneKoanInStorage(chapterNumber, koanOrder);
      setCode(DEFAULT_CODE_VALUE);
      reset();
    }
  };

  const concernedKoans = useMemo(() => {
    return storedKoans.find(
      (passedKoan) =>
        passedKoan.chapterNumber === chapterNumber &&
        passedKoan.koanOrder === koanOrder
    );
  }, [chapterNumber, koanOrder, storedKoans]);

  return (
    <div className={styles.koanPlayerContainer}>
      {isKoanLoading && <div className="one-padding">Koan Fetching...</div>}
      {error && <div className="one-padding">Something wrong : {error}</div>}
      {currentKoan && (
        <>
          <div className={styles.description}>
            <h2>{`${chapterNumber}.${currentKoan.order} ${currentKoan.title}`}</h2>
            <MarkdownDisplayer
              key={`${chapterNumber}-${currentKoan.order}`}
              markdown={currentKoan?.description}
            />
          </div>
          <div className={styles.playground}>
            <div className={styles.editorContainer}>
              {solution.length === 0 ? (
                <CodeMirror
                  value={code}
                  onChange={handleCodeChange}
                  height={deviceSize < 1000 ? "200px" : "65vh"}
                />
              ) : (
                <CodeMirror
                  value={solution}
                  readOnly
                  height={deviceSize < 1000 ? "200px" : "65vh"}
                />
              )}
            </div>

            <div className={styles.buttonContainer}>
              <div>
                <CustomButton
                  disabled={
                    solution.length > 0 ||
                    (concernedKoans && concernedKoans.isPassed) ||
                    (answer && answer.proposeCorrect) ||
                    isMutating
                  }
                  onClick={handleClick}
                  typeButton="success"
                >
                  <>
                    <span>Answer</span>
                    <FontAwesomeIcon icon={faPlay} />
                  </>
                </CustomButton>
              </div>
              <div className={styles.solutionOrResetButtons}>
                {solution.length === 0 ? (
                  <CustomButton
                    onClick={handleShowSolution}
                    typeButton="normal"
                  >
                    <> Solution</>
                  </CustomButton>
                ) : (
                  <CustomButton
                    onClick={handleHideSolution}
                    typeButton="normal"
                  >
                    <>Hide</>
                  </CustomButton>
                )}
                {concernedKoans && (
                  <CustomButton
                    disabled={solution.length > 0 || isMutating}
                    onClick={handleResetPassedKoan}
                    typeButton="normal"
                  >
                    <>Reset</>
                  </CustomButton>
                )}
              </div>
            </div>
            {isMutating && (
              <div
                className={`${styles.resultContainer} ${styles.loadingContainer}`}
              >
                <span>Result is loading</span>
                <FontAwesomeIcon icon={faSpinner} spin />
              </div>
            )}
            {errorAnswer && (
              <div className={styles.resultContainer}>
                Something wrong when send answer {errorAnswer}
              </div>
            )}
            {!isMutating && answer && (
              <div>
                <ResultKoan
                  chapterNumber={chapterNumber}
                  koanOrder={koanOrder}
                  code={code}
                  answer={answer}
                />
              </div>
            )}
          </div>
        </>
      )}
    </div>
  );
};

export const KoanPlayer = memo(KoanPlayerComponent);
