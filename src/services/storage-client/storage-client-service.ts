import { SelectedKoan } from "@/context/selected-koan-context";
import { Chapter } from "@/models/chapter";
import { KoanToStore } from "@/models/store-koans";

export const KOAN_KEY = "KOAN";

export const resetStorage = () => {
  sessionStorage.removeItem(KOAN_KEY);
};

export const getKoansFromStorage = (): KoanToStore[] => {
  const maybeKoans = sessionStorage.getItem(KOAN_KEY);
  return maybeKoans === null ? [] : JSON.parse(maybeKoans);
};

export const removeOneKoanInStorage = (
  chapterNumber: number,
  koanOrder: number
) => {
  const maybeKoans = sessionStorage.getItem(KOAN_KEY);
  if (maybeKoans === null) return;

  const koans: KoanToStore[] = JSON.parse(maybeKoans);
  const filteredKoans = koans.filter(
    (koan) =>
      koan.chapterNumber !== chapterNumber || koan.koanOrder !== koanOrder
  );

  sessionStorage.setItem(KOAN_KEY, JSON.stringify(filteredKoans));
};

export const saveOneKoanInStorage = (koan: KoanToStore) => {
  const koans = getKoansFromStorage();
  const koanIndex = koans.findIndex(
    (storedKoan) =>
      storedKoan.chapterNumber === koan.chapterNumber &&
      storedKoan.koanOrder === koan.koanOrder
  );
  if (koanIndex !== -1) {
    koans.splice(koanIndex, 1, koan);
  } else {
    koans.push(koan);
  }

  sessionStorage.setItem(KOAN_KEY, JSON.stringify(koans));
};

export const getNextKoan = (chapters: Chapter[]): SelectedKoan | undefined => {
  const koans = getKoansFromStorage();

  for (const chapter of chapters) {
    const maybeKoanOrder = getOrderOfKoanNotStoredOrNotPassed(chapter, koans);

    if (maybeKoanOrder) {
      return {
        chapterNumber: chapter.number,
        koanOrder: maybeKoanOrder,
      };
    }
  }
};

const findKoanOrderNotInStorageOrNotPassed = (
  chapter: Chapter,
  koans: KoanToStore[]
) => {
  return chapter.summaryKoans
    .map((summaryKoan) => summaryKoan.order)
    .find((koanOrder) =>
      koans
        .filter((koan) => koan.chapterNumber === chapter.number)
        .every((koan) => koan.koanOrder !== koanOrder || !koan.isPassed)
    );
};

const getOrderOfKoanNotStoredOrNotPassed = (
  chapter: Chapter,
  koans: KoanToStore[]
): number | undefined => {
  const maybeKoanOrder = findKoanOrderNotInStorageOrNotPassed(chapter, koans);
  if (maybeKoanOrder !== undefined) {
    return maybeKoanOrder;
  }

  if (koans.every((koan) => koan.chapterNumber !== chapter.number)) {
    return chapter.summaryKoans[0].order;
  }
};

export const getStoredKoan = (
  chapterNumber: number,
  koanOrder: number
): KoanToStore | undefined => {
  return getKoansFromStorage().find(
    (koan) =>
      koan.chapterNumber === chapterNumber && koan.koanOrder === koanOrder
  );
};
