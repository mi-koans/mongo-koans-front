import { describe, expect, test, afterEach } from "vitest";
import {
  KOAN_KEY,
  getKoansFromStorage,
  getNextKoan,
  getStoredKoan,
  removeOneKoanInStorage,
  saveOneKoanInStorage,
} from "./storage-client-service";
import { Chapter } from "@/models/chapter";
import { SelectedKoan } from "@/context/selected-koan-context";

describe("Storage client service", () => {
  afterEach(() => {
    sessionStorage.clear();
  });

  describe("getKoans", () => {
    test("should get all koans if they are in storage", () => {
      const koans = [
        {
          chapterNumber: 1,
          koanOrder: 1,
          isPassed: true,
          code: "answer 1",
        },
        {
          chapterNumber: 1,
          koanOrder: 1,
          isPassed: true,
          code: "answer 2",
        },
      ];
      sessionStorage.setItem(KOAN_KEY, JSON.stringify(koans));

      expect(getKoansFromStorage()).toEqual(koans);
    });

    test("when there is no koans in storage should return empty array", () => {
      expect(sessionStorage.length).toBe(0);

      expect(getKoansFromStorage()).toEqual([]);
    });
  });

  describe("removeOneKoan", () => {
    test("should remove koan in storage", () => {
      const koans = [
        {
          chapterNumber: 1,
          koanOrder: 1,
          isPassed: true,
          code: "answer 1",
        },
        {
          chapterNumber: 1,
          koanOrder: 2,
          isPassed: true,
          code: "answer 2",
        },
        {
          chapterNumber: 2,
          koanOrder: 1,
          isPassed: false,
          code: "answer 3",
        },
      ];

      sessionStorage.setItem(KOAN_KEY, JSON.stringify(koans));

      removeOneKoanInStorage(1, 2);

      const result = sessionStorage.getItem(KOAN_KEY);
      const expectedResult = [
        {
          chapterNumber: 1,
          koanOrder: 1,
          isPassed: true,
          code: "answer 1",
        },
        {
          chapterNumber: 2,
          koanOrder: 1,
          isPassed: false,
          code: "answer 3",
        },
      ];
      expect(result).toEqual(JSON.stringify(expectedResult));
    });
  });

  describe("saveOneKoanInStorage", () => {
    test("should add when koan is not present", () => {
      const koanToSave = {
        chapterNumber: 1,
        koanOrder: 1,
        code: "code",
        isPassed: false,
      };

      saveOneKoanInStorage(koanToSave);

      const result = sessionStorage.getItem(KOAN_KEY);
      const expectedResult = JSON.stringify([koanToSave]);
      expect(result).toBe(expectedResult);
    });

    test("should update when koans with same chapter number and koan order is already present is storage", () => {
      const savedKoan1 = {
        chapterNumber: 1,
        koanOrder: 2,
        code: "code",
        isPassed: false,
      };
      const savedKoan2 = {
        chapterNumber: 2,
        koanOrder: 2,
        code: "code",
        isPassed: false,
      };
      sessionStorage.setItem(
        KOAN_KEY,
        JSON.stringify([savedKoan1, savedKoan2])
      );
      const koanToUpdate = {
        chapterNumber: 1,
        koanOrder: 2,
        code: "code 2",
        isPassed: true,
      };

      saveOneKoanInStorage(koanToUpdate);

      const result = sessionStorage.getItem(KOAN_KEY);
      const expectedResult = JSON.stringify([koanToUpdate, savedKoan2]);
      expect(result).toBe(expectedResult);
    });
  });

  describe("getNextKoan", () => {
    describe("with list of 2 chapters first one contain 2 koans and 2nd one contain 1 koan", () => {
      const chapters: Chapter[] = [
        {
          name: "chapter 1",
          number: 1,
          summaryKoans: [
            {
              order: 1,
              title: "koan 1 chapter 1",
            },
            {
              order: 2,
              title: "koan 2 chapter 1",
            },
          ],
        },
        {
          name: "chapter 2",
          number: 2,
          summaryKoans: [
            {
              order: 1,
              title: "koan 1 chapter 2",
            },
          ],
        },
      ];

      test("should return koan with first chapter and first order when no koan in storage", () => {
        const selectedKoan = getNextKoan(chapters);

        const expectedResult: SelectedKoan = {
          chapterNumber: 1,
          koanOrder: 1,
        };
        expect(selectedKoan).toEqual(expectedResult);
      });

      test("should return koan with chapter 1 and order 1 when first koan is stored but not passed", () => {
        const savedKoan1 = {
          chapterNumber: 1,
          koanOrder: 1,
          code: "code",
          isPassed: false,
        };
        sessionStorage.setItem(KOAN_KEY, JSON.stringify([savedKoan1]));

        const selectedKoan = getNextKoan(chapters);

        const expectedResult: SelectedKoan = {
          chapterNumber: 1,
          koanOrder: 1,
        };
        expect(selectedKoan).toEqual(expectedResult);
      });

      test("should return koan with chapter 1 and order 2 when first koan is stored", () => {
        const savedKoan1 = {
          chapterNumber: 1,
          koanOrder: 1,
          code: "code",
          isPassed: true,
        };
        sessionStorage.setItem(KOAN_KEY, JSON.stringify([savedKoan1]));

        const selectedKoan = getNextKoan(chapters);

        const expectedResult: SelectedKoan = {
          chapterNumber: 1,
          koanOrder: 2,
        };
        expect(selectedKoan).toEqual(expectedResult);
      });

      test("should return koan with chapter 2 and order 1 when first and second koans are stored", () => {
        const savedKoan1 = {
          chapterNumber: 1,
          koanOrder: 1,
          code: "code",
          isPassed: true,
        };
        const savedKoan2 = {
          chapterNumber: 1,
          koanOrder: 2,
          code: "code 2",
          isPassed: true,
        };
        sessionStorage.setItem(
          KOAN_KEY,
          JSON.stringify([savedKoan1, savedKoan2])
        );

        const selectedKoan = getNextKoan(chapters);

        const expectedResult: SelectedKoan = {
          chapterNumber: 2,
          koanOrder: 1,
        };
        expect(selectedKoan).toEqual(expectedResult);
      });

      test("should return nothing when all koans of all chapters are stored", () => {
        const savedKoan1 = {
          chapterNumber: 1,
          koanOrder: 1,
          code: "code",
          isPassed: true,
        };
        const savedKoan2 = {
          chapterNumber: 1,
          koanOrder: 2,
          code: "code 2",
          isPassed: true,
        };
        const savedLastKoan = {
          chapterNumber: 2,
          koanOrder: 1,
          code: "last code",
          isPassed: true,
        };

        sessionStorage.setItem(
          KOAN_KEY,
          JSON.stringify([savedKoan1, savedKoan2, savedLastKoan])
        );

        expect(getNextKoan(chapters)).toBeUndefined();
      });
    });

    describe("with list of 3 chapters", () => {
      const chapters: Chapter[] = [
        {
          name: "chapter 1",
          number: 1,
          summaryKoans: [
            {
              order: 1,
              title: "koan 1 chapter 1",
            },
            {
              order: 2,
              title: "koan 2 chapter 1",
            },
          ],
        },
        {
          name: "chapter 2",
          number: 2,
          summaryKoans: [
            {
              order: 1,
              title: "koan 1 chapter 2",
            },
          ],
        },
        {
          name: "chapter 3",
          number: 3,
          summaryKoans: [
            {
              order: 1,
              title: "koan 1 chapter 3",
            },
            {
              order: 2,
              title: "koan 2 chapter 3",
            },
            {
              order: 3,
              title: "koan 3 chapter 3",
            },
          ],
        },
      ];

      test("should return koan with chapter 1 and order 2 when first koan is stored", () => {
        const savedKoan1 = {
          chapterNumber: 1,
          koanOrder: 1,
          code: "code",
          isPassed: true,
        };
        const savedKoan3 = {
          chapterNumber: 3,
          koanOrder: 1,
          code: "code",
          isPassed: true,
        };
        sessionStorage.setItem(
          KOAN_KEY,
          JSON.stringify([savedKoan1, savedKoan3])
        );

        const selectedKoan = getNextKoan(chapters);

        const expectedResult: SelectedKoan = {
          chapterNumber: 1,
          koanOrder: 2,
        };
        expect(selectedKoan).toEqual(expectedResult);
      });

      test("should return koan with chapter 1 and order 2 when first koan is stored and passed and other stored koan is not passed", () => {
        const savedKoan1 = {
          chapterNumber: 1,
          koanOrder: 1,
          code: "code",
          isPassed: true,
        };
        const savedKoan3 = {
          chapterNumber: 3,
          koanOrder: 1,
          code: "code",
          isPassed: false,
        };
        sessionStorage.setItem(
          KOAN_KEY,
          JSON.stringify([savedKoan1, savedKoan3])
        );

        const selectedKoan = getNextKoan(chapters);

        const expectedResult: SelectedKoan = {
          chapterNumber: 1,
          koanOrder: 2,
        };
        expect(selectedKoan).toEqual(expectedResult);
      });

      test("should return koan with chapter 2 and order 1 when first and second koans are stored", () => {
        const savedKoan1 = {
          chapterNumber: 1,
          koanOrder: 1,
          code: "code",
          isPassed: true,
        };
        const savedKoan2 = {
          chapterNumber: 1,
          koanOrder: 2,
          code: "code 2",
          isPassed: true,
        };
        sessionStorage.setItem(
          KOAN_KEY,
          JSON.stringify([savedKoan1, savedKoan2])
        );

        const selectedKoan = getNextKoan(chapters);

        const expectedResult: SelectedKoan = {
          chapterNumber: 2,
          koanOrder: 1,
        };
        expect(selectedKoan).toEqual(expectedResult);
      });
    });
  });

  describe("getStoredKoan", () => {
    test("when no koan stored should return undefined", () => {
      expect(getStoredKoan(1, 1)).toBeUndefined();
    });

    test("when one koan stored and correspond to given chapter number and koan order", () => {
      const savedKoan1 = {
        chapterNumber: 1,
        koanOrder: 2,
        code: "code",
        isPassed: false,
      };
      sessionStorage.setItem(KOAN_KEY, JSON.stringify([savedKoan1]));

      expect(getStoredKoan(1, 2)).toEqual(savedKoan1);
    });
  });
});
