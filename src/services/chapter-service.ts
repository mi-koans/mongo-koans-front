import { Chapter } from "../models/chapter";

const API_MONGO_KOANS_CHAPTER_ROUTE = `${process.env.MONGO_KOANS_URL}/chapter`;

export const getAllChapters = async (): Promise<Chapter[]> => {
  try {
    const response = await fetch(API_MONGO_KOANS_CHAPTER_ROUTE);
    if (response.ok) {
      console.log("get all chapters success");
      return response.json();
    } else {
      console.error("problem fetch chapters");
      throw new Error(await response.text());
    }
  } catch (ex: any) {
    console.error("catch exception : ", ex);
    throw new Error(ex);
  }
};

export const getOneKoan = async (chapterNumber: number, koanOrder: number) => {
  const response = await fetch(
    `${API_MONGO_KOANS_CHAPTER_ROUTE}/${chapterNumber}/koan/${koanOrder}`
  );
  if (response.ok) {
    console.log("get one koan success");
    return response.json();
  } else {
    console.error("problem get one koan");
    throw new Error(await response.text());
  }
};

export const proposeAnswer = async (
  commands: string,
  chapterNumber: number,
  koanOrder: number
) => {
  const response = await fetch(
    `${API_MONGO_KOANS_CHAPTER_ROUTE}/${chapterNumber}/koan/${koanOrder}`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ commands }),
    }
  );
  if (response.ok) {
    console.log("propose answer request success");
    return response.json();
  } else {
    console.error("problem propose answer");
    throw new Error(await response.text());
  }
};
