export type SummaryKoans = {
  order: number;
  title: string;
};

export type Chapter = {
  number: number;
  name: string;
  summaryKoans: SummaryKoans[];
};
