export type Koan = {
  order: number;
  title: string;
  description: string;
  solution: string;
};
