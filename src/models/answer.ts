type Answer = {
  commandResult: {
    message: string;
  };
  proposeCorrect: boolean;
};
