export type KoanToStore = {
  chapterNumber: number;
  koanOrder: number;
  isPassed: boolean;
  code: string;
};
