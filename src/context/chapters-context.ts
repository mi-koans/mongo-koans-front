import { Chapter } from "@/models/chapter";
import React from "react";

type ChaptersContextValue = {
  chapters: Chapter[];
};

export const ChaptersContext = React.createContext<ChaptersContextValue>(
  {} as ChaptersContextValue
);
