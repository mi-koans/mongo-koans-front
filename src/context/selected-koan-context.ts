import React, { Dispatch, SetStateAction } from "react";

export type SelectedKoan = {
  koanOrder: number;
  chapterNumber: number;
};
type SelectedKoanContextValue = {
  selectedKoan?: SelectedKoan;
  setSelectedKoan: Dispatch<SetStateAction<SelectedKoan | undefined>>;
};

export const SelectedKoanContext =
  React.createContext<SelectedKoanContextValue>({} as SelectedKoanContextValue);
