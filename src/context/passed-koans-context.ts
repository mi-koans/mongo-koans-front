import React from "react";
import { Dispatch, SetStateAction } from "react";

export type StoredKoan = {
  chapterNumber: number;
  koanOrder: number;
  isPassed: boolean;
};

type StoredKoanContextValue = {
  storedKoans: StoredKoan[];
  setStoredKoans: Dispatch<SetStateAction<StoredKoan[]>>;
};

export const StoredKoansContext = React.createContext<StoredKoanContextValue>(
  {} as StoredKoanContextValue
);
